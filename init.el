;;; init.el --- Emacs initialization  -*- lexical-binding: t; -*-

;; Copyright (C) 2018, 2019, 2020 J. Alexander Branham

;; Licensed under the GNU General Public License v3.0
;; <https://www.gnu.org/licenses/>.

;;; Commentary:
;; This is my personal Emacs config.  It works for me, but probably won't
;; for you.  The general idea is to load a few things early that need to be
;; set early, like the package manager, and then use `use-package' to
;; load/defer/config everything else alphabetically.

;;; Code:

(when (< emacs-major-version 26) ; Minimum version
  (error "Your Emacs is too old -- this config requires version 26 or higher"))

;; Temporarily raise the gc threshold
(setq gc-cons-threshold (* 50 1000 1000))

;;; Early birds
(progn ;     startup & C source code vars
  (setq user-init-file (or load-file-name buffer-file-name)
        user-emacs-directory (file-name-directory user-init-file))
  (message "Loading %s..." user-init-file)
  ;; (package-initialize)
  (setq inhibit-startup-buffer-menu t
        inhibit-startup-screen t
        package-enable-at-startup nil
        load-prefer-newer t
        ;; don't use popup boxes, just make the minibuffer ask
        use-dialog-box nil
        ;; Delete my files by moving them to the trash. I'm human and
        ;; occasionally delete things that I actually want later:
        delete-by-moving-to-trash t
        ;; Emacs has some awful scrolling by default. This gets rid of that.
        scroll-step 1 ; keyboard scroll one line at a time
        scroll-preserve-screen-position 'always
        scroll-conservatively 101
        next-screen-context-lines 5
        debugger-stack-frame-as-list t
        ;; Echo keystrokes a little faster
        echo-keystrokes 0.5
        ;; remove auditory clutter:
        ring-bell-function #'ignore)
  (advice-add #'display-startup-echo-area-message :override #'ignore)
  ;; Don't ever use tabs. Always use spaces.
  (setq-default indent-tabs-mode nil)
  ;; for the lazy:
  (setq use-short-answers t)
  ;; remove visual clutter:
  (scroll-bar-mode 0)
  (tool-bar-mode 0)
  (menu-bar-mode 0)
  ;; Emacs thinks that some new users may find some commands confusing, so
  ;; they're disabled by default. I use these every now and then, so let's
  ;; enable them by default:
  (put #'downcase-region 'disabled nil)
  (put #'upcase-region 'disabled nil)
  (put #'narrow-to-region 'disabled nil)
  ;; Prefer utf8
  (prefer-coding-system 'utf-8))

(setq straight-repository-branch "develop")
(defvar bootstrap-version)
(let ((bootstrap-file
       (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
      (bootstrap-version 6))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
        (url-retrieve-synchronously
         "https://raw.githubusercontent.com/radian-software/straight.el/develop/install.el"
         'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))

(straight-use-package 'use-package)
;; :config
;; ;; Setup a personal keymap. I'll bind various things to this later on:
(bind-keys :prefix "<f1>"
           :prefix-map my/map)
(setq use-package-enable-imenu-support t
      use-package-compute-statistics t)

(use-package auto-compile
  :straight t
  :demand t
  :custom
  (auto-compile-mode-line-counter t "Show compile info in the mode-line")
  (auto-compile-source-recreate-deletes-dest t)
  (auto-compile-toggle-deletes-nonlib-dest t)
  (auto-compile-update-autoloads t)
  (auto-compile-display-buffer nil "Don't display compile buffer")
  :hook
  (auto-compile-inhibit-compile . auto-compile-inhibit-compile-detached-git-head)
  :config
  (auto-compile-on-load-mode)
  (auto-compile-on-save-mode))

;; Finally, I set up no-littering, which keeps my .emacs.d folder clean by
;; putting files into appropriate subfolders rather than letting them get
;; saved all over the place:
(use-package no-littering
  :straight t
  :demand t)

;;; end of early birds, alphabetical from here on out:

(use-package abbrev
  :defer 2
  :custom
  (save-abbrevs 'silently)
  :hook
  ((text-mode python-mode ess-r-mode) . abbrev-mode))

(use-package aggressive-indent
  :straight t
  :defer 10
  :init
  ;; The whole point of this mode is so I don't have to indent things
  ;; manually; why in the world is it setting up a keymap!?
  (setq aggressive-indent-mode-map (make-sparse-keymap))
  :config
  (global-aggressive-indent-mode))

(use-package alert
  ;; Set it up so Emacs can send system notifications:
  :defer t
  :custom
  (alert-default-style 'libnotify)
  :config
  (defun my/remind-me (x y)
    "Remind me in X seconds about thing Y."
    (interactive "sRun after seconds: \nsRemind me about: ")
    (run-with-timer x nil #'alert y))
  (defun my/pause-notifications ()
    "Pause notification display."
    (interactive)
    (alert "foo" :title "DUNST_COMMAND_PAUSE")
    (message "Notifications paused."))
  (defun my/resume-notifications ()
    "Resume notification display."
    (interactive)
    (alert "foo" :title "DUNST_COMMAND_RESUME")
    (message "Notifications resumed.")))

(use-package appt
  ;; keep track of appointments
  :defer 2
  :custom
  (appt-delete-window-function (lambda () t))
  (appt-disp-window-function #'my/appt-display)
  (appt-display-interval 12 "Don't notify more than once")
  (appt-message-warning-time 12)
  (appt-display-mode-line nil)
  :config
  (defun my/appt-display (time-til _time msg)
    (if (listp time-til)
        (dotimes (i (length msg))
          (alert (concat (nth i msg) " in " (nth i time-til) " minutes")
                 :title "Appt"))
      (alert (concat msg " in " time-til " minutes") :title "Appt")))
  (appt-activate))

(use-package async
  :straight t
  ;; Async is written to let things be more async-y in Emacs.  I use it for
  ;; dired-async mode mostly.
  :defer 1
  :custom
  (dired-async-message-function #'my/dired-async-message-function)
  :config
  (defun my/dired-async-message-function (text _face &rest args)
    "Log messages from dired-async to messages buffer."
    ;; For whatever reason, the default for this *doesn't* log it to
    ;; *Messages*.  Instead, it just displays the notification in the
    ;; mode line for 3 seconds, but if you type something it
    ;; immediately goes away.  So just log it to *Messages* like a sane
    ;; person instead:
    (message (format "Finished %s" (apply #'format text args))))
  ;; do dired actions asynchronously
  (dired-async-mode))

(use-package auth-source-pass
  ;; Integrate Emacs's builtin auth-source with pass:
  :if (executable-find "pass")
  :demand t
  :config
  (auth-source-pass-enable))

(use-package autorevert
  :defer 1
  :hook
  (dired-mode . auto-revert-mode)
  :custom
  (auto-revert-verbose nil)
  :config
  ;; Emacs should refresh buffers automatically so if they've changed on
  ;; disk the buffer will update.
  (global-auto-revert-mode))

(use-package bookmark
  :defer
  :config
  (defun my/add-bookmark (file dir)
    "Add FILE in DIR to `bookmark-alist' if not there already."
    (when (not (assoc file bookmark-alist))
      (add-to-list 'bookmark-alist
                   `(,file .
                           ((filename . ,(expand-file-name file dir)))))))
  (my/add-bookmark "finances.ledger" my/ledger-directory)
  (my/add-bookmark "init.el" user-emacs-directory))

(use-package browse-url
  :defer t
  :config
  (setq browse-url-generic-program (or (executable-find "firefox")
                                       (executable-find "qutebrowser"))))

(use-package bug-reference
  :bind
  (:map bug-reference-map
        ("C-c C-o" . bug-reference-push-button)))

(use-package calc
  :defer t
  :bind
  ("<XF86Calculator>" . quick-calc)
  ("C-c =" . quick-calc)
  (:map my/map
        ("C" . my/calc-eval-region)))

(use-package calendar
  ;; Yes, my text editor comes with a calendar built in.  Doesn't yours?
  :defer t
  :hook
  ;; make today easier to find, visually:
  (calendar-today-visible . calendar-mark-today)
  :config
  (setq calendar-location-name "DC")
  (setq calendar-latitude [38 54 north])
  (setq calendar-longitude [77 00 west])
  ;; Show holidays in the calendar
  (setq calendar-mark-holidays-flag t)
  ;; Weeks start on Sunday
  (setq calendar-week-start-day 0)
  (setq calendar-date-display-form calendar-iso-date-display-form)
  (calendar-set-date-style 'iso))

(use-package comint
  :defer t
  ;; comint is the mode from which inferior processes inherit, like the
  ;; python REPL or iESS modes (the R console)
  :config
  (setq comint-prompt-read-only t)
  (setq comint-move-point-for-output nil)
  (setq comint-scroll-to-bottom-on-input 'this))

(use-package comp
  :defer
  :custom
  (native-comp-async-report-warnings-errors 'silent))

(use-package compile
  :defer t
  :bind
  ("<f8>" . compile)
  :hook
  (shell-mode . compilation-shell-minor-mode)
  :custom
  (compilation-ask-about-save nil)
  (compilation-scroll-output 'first-error)
  (compile-command "make -k -j4 "))

(use-package conf-mode
  :mode (("\\.automount\\'" . conf-unix-mode)
         ("\\.link\\'" . conf-unix-mode)
         ("\\.mount\\'" . conf-unix-mode)
         ("\\.netdev\\'" . conf-unix-mode)
         ("\\.network\\'" . conf-unix-mode)
         ("\\.path\\'" . conf-unix-mode)
         ("\\.slice\\'" . conf-unix-mode)
         ("\\.socket\\'" . conf-unix-mode)
         ("\\.target\\'" . conf-unix-mode)
         ("\\.timer\\'" . conf-unix-mode)
         ("\\.service\\'" . conf-unix-mode)))

(use-package counsel
  :straight t
  ;; counsel is closely related to `ivy' and `swiper'.  It provides
  ;; additional keybindings for some commands like `find-file' by remapping
  ;; it to `counsel-find-file'.
  :demand t
  :bind
  (("M-o" . counsel-semantic-or-imenu)
   ("M-s g" . counsel-git-grep)
   ([remap insert-char] . counsel-unicode-char)
   ([remap eshell-previous-matching-input] . counsel-esh-history)
   :map counsel-find-file-map
   ("C-l" . counsel-up-directory)
   :map my/map
   ("t" . counsel-org-goto-all))
  :config
  (setq counsel-find-file-at-point t)
  (counsel-mode))

(use-package csv-mode
  :straight t
  ;; Emacs can handle csv files with ease:
  :defer)

(use-package custom
  :no-require t
  :defer t
  :init
  ;; Don't write customization settings to init.el ... or anywhere else.
  (setq custom-file null-device))

(use-package delsel
  :defer 1
  :config
  ;; Emacs by default doesn't replace selected text if you start typing
  ;; over it.  Since that's the behavior of virtually all other programs,
  ;; let's make emacs do that too:
  (delete-selection-mode))

(use-package diff-hl
  :straight t
  :defer 15
  ;; highlight changes to files on the side
  :hook
  (magit-post-refresh . diff-hl-magit-post-refresh)
  :config
  (global-diff-hl-mode))

(use-package diff-mode
  :config
  (setq diff-font-lock-prettify t))

(use-package dired
  ;; Emacs can act as your file finder/explorer.  Dired is the built-in way
  ;; to do this.
  :defer t
  :bind
  (("C-x C-d" . dired) ; overrides list-directory, which I never use
   :map  dired-mode-map
   ("l" . dired-up-directory)) ; use l to go up in dired
  :config
  (setq dired-auto-revert-buffer t)
  (setq dired-create-destination-dirs 'ask)
  (setq dired-dwim-target t)
  (setq dired-isearch-filenames 'dwim)
  (setq dired-recursive-copies 'always)
  (setq dired-recursive-deletes 'always)
  ;; -l: long listing format REQUIRED in dired-listing-switches
  ;; -a: show everything (including dotfiles)
  ;; -h: human-readable file sizes
  (setq dired-listing-switches "-alh --group-directories-first")
  (defun my/dired-ediff-marked ()
    "Run `ediff' on two marked files in a dired buffer."
    (interactive)
    (unless (eq 'dired-mode major-mode)
      (error "For use in Dired buffers only"))
    (let ((files (dired-get-marked-files)))
      (when (not (eq 2 (length files)))
        (error "Two files not marked"))
      (ediff (car files) (nth 1 files)))))

(use-package ediff
  ;; Ediff is great, but I have to tell it to use one frame (since I start
  ;; Emacs before X/wayland, it defaults to using two frames).
  :defer t
  :custom
  (ediff-window-setup-function #'ediff-setup-windows-plain)
  :hook
  (ediff-prepare-buffer . my/ediff-prepare-buffer)
  :config
  (defun my/ediff-prepare-buffer ()
    "Function to prepare ediff buffers.

Runs with `ediff-prepare-buffer-hook' so that it gets run on all
three ediff buffers (A, B, and C)."
    (when (memq major-mode '(org-mode emacs-lisp-mode))
      ;; unfold org/elisp files
      (outline-show-all))))

(use-package eglot
  :straight t
  :defer t
  :bind
  (:map eglot-mode-map
        ("C-c C-h" . eglot-help-at-point))
  :hook
  (python-mode . eglot-ensure)
  :config
  (setq eglot-autoshutdown t))

(use-package eldoc
  ;; eldoc shows useful information in the minibuffer and is enabled by
  ;; default.
  :defer 1
  :config
  ;; No need to delay showing eldoc
  (setq eldoc-idle-delay 0))

(use-package elec-pair
  :defer 1
  :config
  (electric-pair-mode))

(use-package electric-operator
  :straight t
  ;; Electric operator will turn ~a=10*5+2~ into ~a = 10 * 5 + 2~, so let's
  ;; enable it for R:
  :hook
  ((ess-r-mode python-mode) . electric-operator-mode)
  :custom
  (electric-operator-R-named-argument-style 'spaced))

(use-package elisp-mode
  :defer t
  :bind
  (:map emacs-lisp-mode-map
        ("C-c C-b" . eval-buffer)
        ("C-c C-M-b" . emacs-lisp-byte-compile-and-load))
  :hook
  ;; Turn on flymake for emacs-lisp:
  (emacs-lisp-mode . my/setup-emacs-lisp-mode)
  :config
  (defun my/setup-emacs-lisp-mode ()
    "Setup stuff for elisp."
    ;; Sentences end with a double space in elisp:
    (setq-local sentence-end-double-space t)))

(use-package emacsbug
  :defer t
  :custom
  (report-emacs-bug-no-explanations t))

(use-package epg-config
  :defer
  :custom
  (epg-pinentry-mode 'loopback))
(use-package eshell
  ;; Eshell is Emacs' built-in shell.  You get UNIX-y goodness even on
  ;; Windows machines, plus it can evaluate elisp.
  :defer t
  :custom
  (eshell-buffer-maximum-lines 20000 "Auto truncate after 20k lines")
  (eshell-highlight-prompt nil "My prompt is easy enough to see")
  (eshell-hist-ignoredups t "No duplicates in history")
  (eshell-history-size 1024 "history size")
  (eshell-list-files-after-cd t "List files after cd.")
  (eshell-ls-initial-args "-ah" "Also list all files & human-readable filesizes.")
  (eshell-plain-echo-behavior t "treat 'echo' like shell echo")
  (eshell-prompt-function #'my/eshell-prompt)
  (eshell-prompt-regexp "^λ ")
  (eshell-scroll-to-bottom-on-input 'this)
  (eshell-cmpl-cycle-completions nil)
  :bind
  (("C-c e" . eshell))
  :config
  (defun my/eshell-prompt ()
    "Function that determines the eshell prompt.  Must set
`eshell-prompt-function' to this for it to work."
    (require 'subr-x)
    (require 'vc-git)
    (let ((path (abbreviate-file-name (eshell/pwd))))
      (concat
       ;; working directory
       (format (propertize "(%s)")
               (propertize path 'face '(:inherit eshell-prompt)))
       ;; git info
       (when-let ((branch (and (executable-find "git")
                               (car (vc-git-branches)))))
         (format (propertize "@%s")
                 (propertize branch 'face '(:foreground "light slate grey"))))
       ;; newline, then prompt
       (propertize "\nλ" 'face '(:inherit eshell-prompt))
       ;; need to have a space, otherwise the first text I type gets
       ;; propertized to match λ:
       " "))))

(use-package esh-module
  :defer t
  :config
  ;; Don't show the welcome message banner:
  (delq 'eshell-banner eshell-modules-list)
  ;; use TRAMP sudo method to avoid retyping sudo password on multiple calls:
  (add-to-list 'eshell-modules-list 'eshell-tramp))

(use-package ess
  :straight t
  :bind
  ;; (:map inferior-ess-mode-map
  ;;       ;; Usually I bind C-z to `undo', but I don't really use `undo' in
  ;;       ;; inferior buffers. Use it to switch to the R script (like C-c
  ;;       ;; C-z):
  ;;       ("C-z" . ess-switch-to-inferior-or-script-buffer))
  :custom
  (ess-ask-for-ess-directory nil "Don't ask for dir when starting a process")
  (ess-style 'RStudio)
  (ess-eval-visibly nil "Don't hog Emacs")
  (ess-pdf-viewer-pref "emacsclient")
  (ess-use-ido nil)
  (ess-plain-first-buffername nil "Name first R process R:1")
  (ess-tab-complete-in-script t "TAB should complete.")
  (ess-write-to-dribble nil)
  (ess-auto-width 'window)
  (ess-execute-in-process-buffer t)
  ;; Save R history in one place rather than making .Rhistory files
  ;; everywhere.
  (ess-history-directory (expand-file-name "ESS-history/" no-littering-var-directory))
  (inferior-ess-fix-misaligned-output t)
  :config
  ;; Make history folder if needed.
  (mkdir ess-history-directory t)
  (defun my/emacs-q-with-ess ()
    "Start another Emacs with ESS in `load-path'."
    (interactive)
    (start-process-shell-command "emacs" nil (concat "emacs -Q -L " ess-lisp-directory))))

(use-package ess-r-mode
  :defer
  :bind
  (:map ess-r-mode-map
        ("M-=" . ess-cycle-assign)
        ("M-p" . my/add-pipe)
        ("C-|" . my/ess-eval-pipe-through-line))
  (:map inferior-ess-r-mode-map
        ("M-=" . ess-cycle-assign))
  :config
  (setq inferior-R-args "--no-save")
  (setq ess-R-font-lock-keywords
        '((ess-R-fl-keyword:modifiers . t)
          (ess-R-fl-keyword:fun-defs . t)
          (ess-R-fl-keyword:keywords . t)
          (ess-R-fl-keyword:assign-ops . t)
          (ess-R-fl-keyword:constants . t)
          (ess-fl-keyword:fun-calls . nil)
          (ess-fl-keyword:numbers . t)
          (ess-fl-keyword:operators . t)
          (ess-fl-keyword:delimiters . nil)
          (ess-fl-keyword:= . t)
          (ess-R-fl-keyword:F&T . t)
          (ess-R-fl-keyword:%op% . t)))
  (setq inferior-ess-r-font-lock-keywords
        '((ess-S-fl-keyword:prompt . t)
          (ess-R-fl-keyword:messages . t)
          (ess-R-fl-keyword:modifiers . t)
          (ess-R-fl-keyword:fun-defs . t)
          (ess-R-fl-keyword:keywords . t)
          (ess-R-fl-keyword:assign-ops . t)
          (ess-R-fl-keyword:constants . t)
          (ess-fl-keyword:matrix-labels . t)
          (ess-fl-keyword:fun-calls . nil)
          (ess-fl-keyword:numbers . nil)
          (ess-fl-keyword:operators . nil)
          (ess-fl-keyword:delimiters . nil)
          (ess-fl-keyword:= . nil)
          (ess-R-fl-keyword:F&T . nil)))
  (defun my/add-pipe ()
    "Add a pipe operator %>% at the end of the current line.
Don't add one if the end of line already has one.  Ensure one
space to the left and start a newline with indentation."
    (interactive)
    (end-of-line)
    (unless (looking-back "|>" nil)
      (just-one-space 1)
      (insert "|>"))
    (newline-and-indent))
  (defun my/advice-ess-cycle-assign ()
    "Call `count-words-region' iff the region is active."
    (when (use-region-p)
      ;; count-words--message echos it to the minibuffer, which is what I want.
      (count-words--message "Region" (region-beginning) (region-end))))
  (advice-add 'ess-cycle-assign :before-until 'my/advice-ess-cycle-assign)
  ;; I sometimes want to evaluate just part of a piped sequence. The
  ;; following lets me do so without needing to insert blank lines or
  ;; something:
  (defun my/ess-beginning-of-pipe-or-end-of-line ()
    "Find point position of end of line or beginning of pipe %>%."
    (if (search-forward "|>" (line-end-position) t)
        (goto-char (match-beginning 0))
      (end-of-line)))
  (defun my/ess-eval-pipe-through-line (vis)
    "Like `ess-eval-paragraph' but only evaluates up to the pipe on this line.

If no pipe, evaluate paragraph through the end of current line.

Prefix arg VIS toggles visibility of ess-code as for `ess-eval-region'."
    (interactive "P")
    (save-excursion
      (let ((end (progn
                   (my/ess-beginning-of-pipe-or-end-of-line)
                   (point)))
            (beg (progn (backward-paragraph)
                        (ess-skip-blanks-forward 'multiline)
                        (point))))
        (ess-eval-region beg end vis)))))

(use-package exec-path-from-shell
  :straight t
  ;; This ensures Emacs has the same PATH as the rest of my system.  It is
  ;; necessary for macs (not that I ever use that), or if Emacs is started
  ;; via a systemd service, as systemd user services don't inherit the
  ;; environment of that user
  :if (or (eq system-type 'darwin)
          (and (daemonp)
               (eq system-type 'gnu/linux)))
  :config
  (exec-path-from-shell-initialize))

(use-package executable
  :hook
  ;; Emacs can set file permissions automatically.  Make scripts executable
  ;; so I don't have to remember to do so:
  (after-save . executable-make-buffer-file-executable-if-script-p))

(use-package eww
  :commands (eww eww-search-words)
  :bind
  ;; If a webpage requires more than eww can handle, I can switch to the
  ;; system default by tapping &, but 0 is easier to type:
  (:map eww-mode-map
        ("0" . eww-browse-with-external-browser)))

(use-package faces
  ;; faces are how Emacs determines how to display characters (font, size,
  ;; color, etc)
  :defer t
  :bind
  ("C-h c" . describe-face) ; overrides describe-key-briefly from help.el
  :custom-face
  (cursor ((t :background "#abb2bf")))
  (help-argument-name ((t (:inherit font-lock-doc-face))))
  :config
  (add-to-list 'default-frame-alist
               '(font . "monospace-12")))

(use-package face-remap
  :bind
  ;; Everywhere else you can zoom with C-- and C-+.  Let's make Emacs
  ;; follow that convention:
  ("C-=" . text-scale-increase)
  ("C-+" . text-scale-increase)
  ("C--" . text-scale-decrease))

(progn ; `files.el'
  (setq backup-directory-alist `(("." . ,(expand-file-name "backups" user-emacs-directory))))
  ;; Don't ask me when I try to create a new file.  Just create it.
  (setq confirm-nonexistent-file-or-buffer nil)
  ;; make final newlines if they don't exist:
  (setq require-final-newline t)

  ;; C-x C-c is originally bound to kill emacs. I accidentally type this
  ;; from time to time which is super-frustrating.  Get rid of it:
  (unbind-key "C-x C-c")
  (bind-key "s-s" #'save-buffer)
  ;; `auto-save-visited-mode' was added in Emacs 26 and auto saves files
  ;; every `auto-save-visited-interval' seconds.
  (auto-save-visited-mode)
  ;; Don't ask about saving files in `save-some-buffers', just save them
  (advice-add 'save-some-buffers :around
              (lambda (oldfun &optional arg pred)
                (funcall oldfun t pred)))



  ;; This lets me make directories on the fly similar to mkdir -p. Thanks
  ;; --- http://mbork.pl/2016-07-25_Making_directories_on_the_fly
  (defun my/make-parent-directory ()
    "Make sure the directory of `buffer-file-name' exists."
    (make-directory (file-name-directory buffer-file-name) t))
  (add-hook 'find-file-not-found-functions #'my/make-parent-directory)
  ;; Sometimes stuff gets out of whack, this helps me put it back in whack:
  (defun my/save-and-revert-buffer ()
    "Save and then revert this buffer."
    (interactive)
    (progn
      (save-buffer)
      (revert-buffer nil t)))
  (bind-key "G" #'my/save-and-revert-buffer my/map)
  ;; Prompt me to save changed buffers if I'm closing the last frame (and
  ;; Emacs is running as a daemon):
  (when (daemonp)
    (add-to-list 'delete-frame-functions
                 (defun my/save-if-last-frame (frame)
                   (when (eq 1 (cl-count-if
                                (lambda (f)
                                  (eq
                                   (frame-parameter f 'display)
                                   (frame-parameter frame 'display)))
                                (visible-frame-list)))
                     (save-some-buffers))))))

(use-package find-func
  :defer
  :bind
  ("C-c f f" . find-function)
  ("C-c f v" . find-variable)
  ("C-c f l" . find-library)
  :hook
  (find-function-after . reposition-window))

(use-package flymake
  :defer t
  :custom
  (flymake-proc-compilation-prevents-syntax-check nil)
  :hook
  ;; Some modes turn `flymake-mode' on by default, I have to tell these
  ;; specifically to do it:
  ((emacs-lisp-mode python-mode LaTeX-mode). flymake-mode)
  :bind
  (:map flymake-mode-map
        ("M-P" . flymake-goto-prev-error)
        ("M-N" . flymake-goto-next-error))
  :config
  (remove-hook 'flymake-diagnostic-functions #'flymake-proc-legacy-flymake))

(use-package flyspell
  ;; on the fly spell checking
  :hook
  (text-mode . turn-on-flyspell)
  (prog-mode . flyspell-prog-mode)
  :custom
  (flyspell-use-meta-tab nil)
  (flyspell-abbrev-p t)
  (flyspell-issue-welcome-flag nil)
  (flyspell-use-global-abbrev-table-p t))

(use-package footnote
  :defer
  :hook
  (message-mode . footnote-mode))

(use-package frame
  :defer t
  :config
  ;; Don't blink the cursor. It's annoying.
  (blink-cursor-mode -1)
  ;; don't bind C-x C-z to suspend-frame:
  (unbind-key "C-x C-z")
  ;; In fact, I find suspend-frame so unhelpful let's disable it:
  (put #'suspend-frame 'disabled t)
  (defun my/setup-theme ()
    "Setup some appearance optione."
    (when (window-system)
      (set-face-attribute 'variable-pitch nil :family "Linux Libertine O")
      (load-theme 'misterioso t)))
  (if (daemonp)
      (add-hook 'after-make-frame-functions #'my/setup-theme)
    (my/setup-theme)))

(use-package goto-addr
  :bind
  (:map goto-address-highlight-keymap
        ("C-c C-o" . goto-address-at-point))
  :hook
  ((erc-mode eshell-mode shell-mode comint-mode) . goto-address-mode)
  (prog-mode . goto-address-prog-mode))

(use-package help
  ;; Emacs has an amazing help system built in. C-h v, C-h f, and C-h k are
  ;; bound to describe-variable, describe-function, and describe-key
  ;; respectively.
  :no-require t
  :bind
  (:map help-mode-map
        ;; shortcuts for searching from *Help* buffers
        ("v" . describe-variable)
        ("f" . describe-function)
        ("k" . describe-key))
  :custom
  ;; This makes emacs switch focus to help windows:
  (help-window-select t))

(use-package hippie-exp
  :bind
  (("M-SPC" . hippie-expand)
   ([remap dabbrev-expand] . hippie-expand))
  :init
  (setq hippie-expand-try-functions-list
        '(;; Try to expand word "dynamically", searching the current buffer.
          try-expand-dabbrev
          ;; Try to expand word "dynamically", searching all other buffers.
          try-expand-dabbrev-all-buffers
          ;; Try to complete text as a file name, as many characters as unique.
          try-complete-file-name-partially
          ;; Try to complete text as a file name.
          try-complete-file-name
          ;; Try to expand word "dynamically", searching the kill ring.
          try-expand-dabbrev-from-kill
          ;; Try to complete the current line to an entire line in the buffer.
          try-expand-list
          ;; Try to complete the current line to an entire line in the buffer.
          try-expand-line))
  (setq hippie-expand-verbose nil))

(use-package hl-line
  :defer
  :hook
  ((tabulated-list-mode
    org-agenda-mode timer-list-mode profiler-report-mode vc-annotate-mode
    gnus-group-mode ledger-report-mode)
   . hl-line-mode))

(use-package holidays
  :defer t
  :init
  ;; Emacs knows about holidays, but there are lots that don't affect me.
  ;; Let's hide them
  (setq holiday-bahai-holidays nil
        holiday-hebrew-holidays nil
        holiday-islamic-holidays nil
        holiday-oriental-holidays nil
        holiday-christian-holidays nil))

(use-package imenu
  :defer t
  :hook
  (imenu-after-jump . recenter)
  :config
  (setq imenu-auto-rescan t)
  (setq imenu-auto-rescan-maxout 600000))

(progn ; indent.el
  (setq tab-always-indent 'complete)
  (setq tab-first-completion 'word-or-paren-or-punct))

(use-package info
  :defer
  :custom-face
  (Info-quoted ((t (:foreground "MediumPurple" :inherit fixed-pitch-serif)))))

(use-package isearch
  ;; isearch is the package that provides Emacs's forward and reverse
  ;; searching.  These are bound to =C-s= and =C-r= by default.  If you've
  ;; already started a search with =C-s=, then backspace sometimes doesn't
  ;; delete characters; it goes back to the previous match.  I prefer
  ;; backspace to always delete characters; I can just =C-r= to get to the
  ;; previous match.
  :bind
  (:map isearch-mode-map
        ("<backspace>" . isearch-del-char))
  :config
  (setq lazy-highlight-initial-delay 0.05)
  (setq isearch-lazy-count t)
  ;; Use regex searches by default:
  (setq search-default-mode t))

(use-package ivy
  :bind
  ("C-M-z" . ivy-resume)
  ([remap list-buffers] . ivy-switch-buffer)
  :config
  (setq ivy-count-format "(%d/%d) ")
  (setq ivy-use-virtual-buffers t)
  (setq ivy-extra-directories '("./"))
  (dolist (fun '(org-refile org-agenda-refile org-capture-refile))
    (setq ivy-initial-inputs-alist
          (delete `(,fun . "^") ivy-initial-inputs-alist)))
  (ivy-mode))

(use-package ledger-mode
  :straight t
  :if (executable-find "ledger")
  :hook
  (ledger-mode . ledger-flymake-enable)
  (ledger-report-after-report . my/setup-ledger-report)
  :bind
  (:map ledger-mode-map
        ("C-c u" . ledger-navigate-next-uncleared)
        ("C-c U" . ledger-navigate-previous-uncleared)
        ("C-c C-v" . ledger-mode-clean-buffer)
        ("`" . ledger-report)
        :map ledger-report-mode-map
        ("n" . next-line)
        ("p" . previous-line))
  :custom
  (ledger-default-date-format "%F" "ISO dates.")
  (ledger-flymake-be-explicit t "Warn about account typos.")
  (ledger-flymake-be-pedantic t "Warn about account typos.")
  (ledger-post-amount-alignment-at :decimal "Align at decimal place.")
  (ledger-post-amount-alignment-column 70 "Align at column 70")
  (ledger-post-auto-align nil)
  (ledger-report-resize-window nil "Don't resize windows.")
  (ledger-report-use-header-line t "Write info in the `header-line', leaving buffer for reports.")
  (ledger-mode-should-check-version nil "Assume ledger is up-to-date.")
  (ledger-copy-transaction-insert-blank-line-after t)
  :init
  (defvar my/ledger-directory (expand-file-name "Finances" "~/Sync/"))
  :config
  (require 'iso8601)
  (setq ledger-schedule-file (expand-file-name "scheduled.ledger" my/ledger-directory)
        ledger-schedule-look-backward 0
        ledger-schedule-look-forward 31)
  (setq ledger-reports
        (let ((tomorrow (format-time-string "%F" (time-add nil (* 24 3600)))))
          (mapcar (lambda (r) (list (car r)
                               (format "%s %s"
                                       "%(binary) -f %(ledger-file)"
                                       (cdr r))))
                  `(("on-hand"             . ,(concat "balance assets liabilities --exchange $ --end " tomorrow))
                    ("account"             . "register %(account)\\$ --effective")
                    ("expenses (monthly)"  . "register expenses --limit \"commodity == '$' \" -M ")
                    ("expenses (yearly)"   . "register expenses --limit \"commodity == '$' \" -Y ")
                    ("cash-flow-monthly"   . "balance income expenses --exchange $ --invert --period %(month)")
                    ("cash-flow-YTD"       . "balance income expenses --exchange $ --invert -b \"this year\"")
                    ("net-worth-by-day"    . ,(concat "-X '$' --daily register assets liabilities --collapse -F '%(date) %12(T)\n' --end "
                                                      tomorrow))
                    ("payee"               . "reg @%(payee)")
                    ("taxes by year"       . "balance expenses:tax --pivot Tax-Year")))))
  (defun my/ledger-report--header-function ()
    "Use in place of `ledger-report--header-function'."
    (save-match-data
      ;; Remove stuff before .ledger, probably the binary and ledger file.
      (format "Ledger Report: %s"
              (propertize (progn (string-match "\\.ledger " ledger-report-cmd)
                                 (substring ledger-report-cmd (match-end 0)))
                          'face 'font-lock-comment-face))))
  (setq ledger-report-header-line-fn #'my/ledger-report--header-function)


  (defun my/ledger-accounts-excluded-accounts-function (i)
    (let* ((tail (cdr i))
           asserts assert-date)
      (setq asserts (cl-remove-if-not (lambda (j) (equal "assert" (car j))) tail))
      (catch 'exclude
        (dolist (assert asserts)
          (when (and (string-match (rx "date" (0+ whitespace)
                                       "<" (opt "=") (0+ whitespace) "["
                                       (group-n 1 (1+ num) (or (any "/" "-")) (1+ num)
                                                (or (any "-" "/")) (1+ num)) "]")
                                   (cdr assert))
                     (match-string 1 (cdr assert)))
            (throw 'exclude (time-less-p (iso8601-parse (concat (match-string 1 (cdr assert)) "T01:01:01"))
                                         (current-time))))))))
  (setq ledger-accounts-exclude-function #'my/ledger-accounts-excluded-accounts-function)
  (defun my/setup-ledger-report ()
    "Hooks into `ledger-report-after-report-hook'."
    (setq-local truncate-partial-width-windows t)
    (goto-char (point-max))
    (forward-line -1)))

(use-package lisp
  :defer
  :bind
  ("C-c )" . delete-pair)
  :config
  (setq delete-pair-blink-delay 0))

(use-package magit
  :straight t
  :bind
  (("C-x g" . magit-status)
   ("C-x M-g b" . magit-blame-addition)
   ("C-x M-g f" . magit-log-buffer-file)
   :map dired-mode-map
   ("h" . magit-dired-log))
  :init
  (setq magit-define-global-key-bindings nil)
  :config
  ;; Get highlighted word diffs.
  (setq magit-diff-refine-hunk 'all
        magit-display-buffer-function #'display-buffer
        magit-save-repository-buffers 'dontask
        magit-revision-show-gravatars t)
  (magit-wip-mode)
  (magit-add-section-hook 'magit-status-sections-hook
                          'magit-insert-modules
                          'magit-insert-stashes
                          'append))

(use-package markdown-mode
  :straight t
  ;; Markdown mode for Markdown editing!
  :mode (("README\\.md\\'" . gfm-mode)
         ("\\.md\\'" . markdown-mode)
         ("\\.Rmd\\'" . markdown-mode)
         ("\\.markdown\\'" . markdown-mode))
  :bind
  (:map markdown-mode-map
        ("M-p" . markdown-previous-visible-heading)
        ("M-n" . markdown-next-visible-heading))
  :config
  (setq markdown-enable-math t
        markdown-fontify-code-blocks-natively t))

(use-package menu-bar
  :bind
  (:map my/map
        ("d" . toggle-debug-on-error)))

(use-package message
  :defer t
  :custom
  (user-mail-address "alex.branham@gmail.com")
  (user-full-name "Alex Branham")
  (message-kill-buffer-on-exit t)
  ;; next two are from:
  ;; http://pragmaticemacs.com/emacs/customise-the-reply-quote-string-in-mu4e/ :
  ;; customize the reply-quote-string
  (message-citation-line-format "On %a %d %b %Y at %R, %f wrote:\n")
  ;; choose to use the formatted string
  (message-citation-line-function #'message-insert-formatted-citation-line)
  :config
  (when (>= emacs-major-version 27)
    (add-hook 'message-send-hook #'message-sign-encrypt-if-all-keys-available)))

(use-package minibuffer
  :defer t
  :custom
  (read-file-name-completion-ignore-case t "Ignore file case when trying to find stuff:"))

(use-package minions
  :straight t
  ;; Set up how minor modes are displayed in the mode-line
  :custom
  (minions-mode-line-lighter "…" "Don't wink at me.")
  :hook
  (after-init . minions-mode)
  :config
  ;; "I always want flymake-mode visible because it shows counts of errors/warnings."
  (setq minions-prominent-modes2 '(flymake-mode ess-r-package-mode)))

(use-package mixed-pitch
  :straight t
  :bind
  (:map my/map
        ("f" . mixed-pitch-mode)))

(use-package moody
  :straight t
  ;; Set up the `mode-line'
  :demand
  :config
  (setq x-underline-at-descent-line t)
  (setq moody-mode-line-height (if      ; Emacs 26 support
                                   (and (< emacs-major-version 27)
                                        (string= (system-name) "earth"))
                                   40
                                 20))
  (defvar-local my/exwm-and-winum-modeline
    '(:eval (let ((e (when (bound-and-true-p exwm-workspace-current-index)
                       (propertize (number-to-string exwm-workspace-current-index)
                                   'face 'font-lock-type-face)))
                  (w (when winum-mode (winum-get-number-string))))
              (format (concat " " e "|" w " ")))))
  (put 'my/exwm-and-winum-modeline 'risky-local-variable t)
  (moody-replace-element 'mode-line-front-space 'my/exwm-and-winum-modeline)
  (moody-replace-mode-line-buffer-identification)
  (moody-replace-vc-mode))

(use-package mouse
  :defer t
  :bind
  ;; We can use shift-mouse for selecting from point:
  ("<S-down-mouse-1>" . mouse-save-then-kill)
  :custom
  (mouse-yank-at-point t ))

(use-package mwheel
  :defer t
  :custom
  (mouse-wheel-scroll-amount '(1 ((shift) . 1)) "One line at a time.")
  (mouse-wheel-progressive-speed nil "Don't accelerate scrolling.")
  (mouse-wheel-follow-mouse 't "Scroll window under mouse."))

(use-package my-secrets
  :load-path "~/Sync/emacs")

(use-package ob-core
  ;; ob is org-babel, which lets org know about code and code blocks
  :defer t
  :custom
  ;; I know what I'm getting myself into.
  (org-confirm-babel-evaluate nil "Don't ask to confirm evaluation."))

(use-package org
  ;; Org mode is a great thing. I use it for writing academic papers,
  ;; managing my schedule, managing my references and notes, writing
  ;; presentations, writing lecture slides, and pretty much anything
  ;; else.
  :bind
  (("C-c l" . org-store-link)
   ("C-'" . org-cycle-agenda-files) ; quickly access agenda files
   :map org-mode-map
   ("C-a" . org-beginning-of-line)
   ("C-e" . org-end-of-line)
   ;; Bind M-p and M-n to navigate heading more easily (these are bound to
   ;; C-c C-p/n by default):
   ("M-p" . my/org-previous-visible-heading)
   ("M-n" . my/org-next-visible-heading)
   ;; C-c C-t is bound to `org-todo' by default, but I want it
   ;; bound to C-c t as well:
   ("C-c t" . org-todo)
   :map org-read-date-minibuffer-local-map
   ("C-n" . (lambda () (interactive) (org-eval-in-calendar '(calendar-forward-week 1))))
   ("C-p" . (lambda () (interactive) (org-eval-in-calendar '(calendar-backward-week 1))))
   ("C-f" . (lambda () (interactive) (org-eval-in-calendar '(calendar-forward-day 1))))
   ("C-b" . (lambda () (interactive) (org-eval-in-calendar '(calendar-backward-day 1)))))
  :hook
  (org-mode . my/setup-org-mode)
  :custom
  (org-blank-before-new-entry nil)
  (org-cycle-separator-lines 0)
  (org-pretty-entities t "UTF8 all the things!")
  (org-support-shift-select t "Holding shift and moving point should select things.")
  (org-fontify-quote-and-verse-blocks t "Provide a special face for quote and verse blocks.")
  (org-M-RET-may-split-line nil "M-RET may never split a line.")
  (org-enforce-todo-dependencies t "Can't finish parent before children.")
  (org-enforce-todo-checkbox-dependencies t "Can't finish parent before children.")
  (org-hide-emphasis-markers t "Make words italic or bold, hide / and *.")
  (org-catch-invisible-edits 'show-and-error "Don't let me edit things I can't see.")
  (org-special-ctrl-a/e t "Make C-a and C-e work more like how I want:.")
  (org-preview-latex-default-process 'imagemagick "Let org's preview mechanism use imagemagick instead of dvipng.")
  ;; Let imenu go deeper into menu structure
  (org-imenu-depth 6)
  (org-image-actual-width '(300))
  (org-blank-before-new-entry '((heading . nil)
                                (plain-list-item . nil)))
  ;; For whatever reason, I have to explicitely tell org how to open pdf
  ;; links.  I use pdf-tools.  If pdf-tools isn't installed, it will use
  ;; doc-view (shipped with Emacs) instead.
  (org-file-apps
   '((auto-mode . emacs)
     ("\\.mm\\'" . default)
     ("\\.x?html?\\'" . default)
     ("\\.pdf\\'" . emacs)))
  (org-highlight-latex-and-related '(latex entities) "set up fontlocking for latex")
  (org-startup-with-inline-images t "Show inline images.")
  (org-log-done 'time)
  (org-goto-interface 'outline-path-completion)
  (org-ellipsis "⬎")
  (org-tag-persistent-alist '(("jobs" . ?j)
                              (:startgroup . nil)
                              ("@work" . ?w)
                              ("@home" . ?h)
                              (:endgroup . nil)))
  ;; I keep my recipes in an org file and tag them based on what kind of
  ;; dish they are.  The level one headings are names, and each gets two
  ;; level two headings --- ingredients and directions.  To easily search via
  ;; tag, I can restrict org-agenda to that buffer using < then hit m to
  ;; match based on a tag.
  (org-tags-exclude-from-inheritance
   '("BREAKFAST" "DINNER" "DESSERT" "SIDE" "CHICKEN" "PORK" "SEAFOOD"
     "BEEF" "PASTA" "SOUP" "SNACK" "DRINK" "LAMB" "VEGETARIAN"))
  ;; Org-refile lets me quickly move around headings in org files.  It
  ;; plays nicely with org-capture, which I use to turn emails into TODOs
  ;; easily (among other things, of course)
  (org-outline-path-complete-in-steps nil)
  (org-refile-allow-creating-parent-nodes 'confirm)
  (org-refile-use-outline-path 'file)
  :custom-face
  (org-block ((t (:inherit default))))
  :config
  (setq org-refile-targets '((nil . (:level . 1)) ; current file
                             (org-default-notes-file . (:maxlevel . 6))
                             (my/org-scheduled . (:level . 1))))
  ;; These are the programming languages org should teach itself:
  (org-babel-do-load-languages
   'org-babel-load-languages
   '((emacs-lisp . t)
     (latex . t)
     (python . t)
     (R . t)
     (shell . t)))
  ;; remove C-c [ from adding org file to front of agenda
  (unbind-key "C-c [" org-mode-map)
  (defun my/setup-org-mode ()
    "Setup org-mode."
    ;; Automatically export recipes to HTML
    (when (string= buffer-file-name (expand-file-name "recipes.org" "~/Sync/recipes/"))
      (add-hook 'after-save-hook #'org-html-export-to-html nil t))
    ;; An alist of symbols to prettify, see `prettify-symbols-alist'.
    ;; Whether the symbol actually gets prettified is controlled by
    ;; `org-pretty-compose-p', which see.
    (setq-local prettify-symbols-unprettify-at-point nil)
    (setq-local prettify-symbols-alist '(("*" . ?•)))
    (setq-local prettify-symbols-compose-predicate #'my/org-pretty-compose-p))
  (defun my/org-next-visible-heading (arg)
    "Go to next heading and beginning of line."
    (interactive "p")
    (org-next-visible-heading arg)
    (org-beginning-of-line))
  (defun my/org-previous-visible-heading (arg)
    "Go to previous heading and beginning of line."
    (interactive "p")
    (org-previous-visible-heading arg)
    (org-beginning-of-line))
  (defun my/org-pretty-compose-p (start end match)
    "Return t if the symbol should be prettified.
START and END are the start and end points, MATCH is the string
match.  See also `prettify-symbols-compose-predicate'."
    (if (string= match "*")
        ;; prettify asterisks in headings
        (and (org-match-line org-outline-regexp-bol)
             (< end (match-end 0)))
      ;; else rely on the default function
      (prettify-symbols-default-compose-p start end match))))

(use-package org-agenda
  ;; Here's where I set which files are added to org-agenda, which controls
  ;; org's global todo list, scheduling, and agenda features.  I use
  ;; Syncthing to keep these files in sync across computers.
  :bind
  (("C-c a" . org-agenda)
   ("<f5>" . org-agenda)
   :map org-agenda-mode-map
   ;; overrides org-agenda-redo, which I use "g" for anyway
   ("r" . org-agenda-refile)
   ;; overrides saving all org buffers, also bound to C-x C-s
   ("s" . org-agenda-schedule)
   ;; overrides org-exit
   ("x" . my/org-agenda-mark-done))
  :custom
  (org-agenda-skip-deadline-if-done t "Remove done deadlines from agenda.")
  (org-agenda-skip-scheduled-if-done t "Remove done scheduled from agenda.")
  (org-agenda-skip-timestamp-if-done t "Don't show timestamped things in agenda if they're done.")
  (org-agenda-skip-scheduled-if-deadline-is-shown 'not-today "Don't show scheduled if the deadline is visible unless it's also scheduled for today.")
  (org-agenda-skip-deadline-prewarning-if-scheduled 'pre-scheduled "Skip deadline warnings if it is scheduled.")
  (org-deadline-warning-days 3 "warn me 3 days before a deadline")
  (org-agenda-tags-todo-honor-ignore-options t "Ignore scheduled items in tags todo searches.")
  (org-agenda-tags-column 'auto)
  (org-agenda-window-setup 'only-window "Use current window for agenda.")
  (org-agenda-restore-windows-after-quit t "Restore previous config after I'm done.")
  (org-agenda-span 'day) ; just show today. I can "vw" to view the week
  (org-agenda-time-grid
   '((daily today remove-match) (800 1000 1200 1400 1600 1800 2000)
     "" "") "By default, the time grid has a lot of ugly '-----' lines. Remove those.")
  (org-agenda-scheduled-leaders '("" "%2dx ") "I don't need to know that something is scheduled.  That's why it's appearing on the agenda in the first place.")
  (org-agenda-block-separator ?— "Use nice unicode character instead of ugly = to separate agendas:")
  (org-agenda-deadline-leaders '("Deadline: " "In %d days: " "OVERDUE %d day: ") "Make deadlines, especially overdue ones, stand out more:")
  (org-agenda-current-time-string "⸻ NOW ⸻")
  ;; The agenda is ugly by default. It doesn't properly align items and it
  ;; includes weird punctuation. Fix it:
  (org-agenda-prefix-format '((agenda . "%-12c%-14t%s")
                              (todo . " %i %-12:c")
                              (tags . " %i %-12:c")
                              (search . " %i %-12:c")))
  (org-agenda-custom-commands
   '(
     ("h" "Home Agenda"
      ((agenda "" nil)
       (tags "@home"
             ((org-agenda-overriding-header "Tasks to do at home")))
       (tags "CATEGORY=\"inbox\"+LEVEL=2"
             ((org-agenda-overriding-header "Refile")))))
     ("w" "Work Agenda"
      ((agenda "" nil)
       (tags "@work"
             ((org-agenda-overriding-header "Tasks to do at work")))
       (tags "+CATEGORY=\"inbox\"+LEVEL=2"
             ((org-agenda-overriding-header "Refile")))))
     ("d" "deadlines"
      ((agenda ""
               ((org-agenda-entry-types '(:deadline))
                (org-agenda-span 'fortnight)
                (org-agenda-time-grid nil)
                (org-deadline-warning-days 0)
                (org-agenda-skip-deadline-prewarning-if-scheduled nil)
                (org-agenda-skip-deadline-if-done nil)))))
     ("b" "bibliography"
      ((tags "CATEGORY=\"bib\"+LEVEL=2"
             ((org-agenda-overriding-header "")))))
     ("u" "unscheduled"
      ((todo  "TODO"
              ((org-agenda-overriding-header "Unscheduled tasks")
               (org-agenda-todo-ignore-with-date t)))))))
  :init
  (setq org-directory "~/org/")
  (setq org-default-notes-file (concat org-directory "todo.org"))
  (defvar my/org-notes (expand-file-name "notes.org" org-directory)
    "Long-term storage for notes.")
  (defvar my/org-scheduled (expand-file-name "scheduled.org" org-directory)
    "Scheduled tasks.")
  ;; set up org agenda files for the agenda
  (setq org-agenda-files `(,org-default-notes-file
                           ,my/org-scheduled))
  (setq org-agenda-text-search-extra-files `(,my/org-notes))
  :config
  (run-with-timer 1 3600 (lambda () (let ((inhibit-message t))
                                 (org-agenda-to-appt t))))
  (defun my/org-agenda-mark-done (&optional _arg)
    "Mark current TODO as DONE.
See `org-agenda-todo' for more details."
    (interactive "P")
    (org-agenda-todo "DONE")))

(use-package org-capture
  ;; I use org-capture to create short notes about all kinds of things.  I
  ;; can capture emails to remember for later, quick thoughts for later,
  ;; RSS feeds, really anything.
  :bind*
  ("C-c c" . org-capture)
  :bind
  (:map org-capture-mode-map
        ("C-c C-j" . my/org-capture-refile-and-jump))
  :custom
  ;; And now for the capture templates themselves.  It's a bit complicated,
  ;; but the manual does a great job explaining.
  (org-capture-templates
   `(("s" "store" entry (file+headline org-default-notes-file "Inbox")
      "* TODO %?\n:PROPERTIES:\n:CREATED: %U\n:END:\n%a \n%i")
     ("t" "task" entry (file+headline org-default-notes-file "Inbox")
      "* TODO %? \n:PROPERTIES:\n:CREATED: %U\n:END:\n%i")
     ("n" "note" entry (file ,my/org-notes)
      "* %?\n:PROPERTIES:\n:CREATED: %U\n:END:\n %i")
     ("b" "bib" entry (file+headline org-default-notes-file "Bibliography")
      "* TODO %a            :@work:\n\n:PROPERTIES:\n:CREATED: %U\n:END:\n \n %i")
     ("p" "Protocol" entry (file+headline org-default-notes-file "Inbox")
      "* TODO %:annotation\n:PROPERTIES:\n:CREATED: %U\n:END:\n%i" :immediate-finish t))))

(use-package org-indent
  ;; org-indent-mode nicely aligns text with the outline level
  :hook
  (org-mode . org-indent-mode))

(use-package org-protocol
  ;; (info "(org)Protocols")
  :defer 3
  :config
  (setq org-protocol-default-template-key "p"))

(use-package org-src
  ;; org source code examples
  :defer t
  :custom
  (org-src-tab-acts-natively t "This will make the tab key act like you want it to inside code blocks.")
  (org-src-window-setup 'current-window "Set up src windows in their current window rather than another one."))

(use-package outline
  :defer t
  :bind
  (:map outline-mode-map
        ("M-p" . outline-previous-visible-heading)
        ("M-n" . outline-next-visible-heading)))

(use-package ox
  ;; ox is org's export engine
  :defer t
  :custom
  (org-export-with-smart-quotes t)
  ;; Don't include a table of contents when exporting.
  (org-export-with-toc nil))

(use-package ox-html
  :defer t
  :config
  (setq org-html-validation-link nil))

(use-package ox-latex
  ;; org's latex/pdf exporting engine
  :defer t
  :custom
  (org-latex-pdf-process '("latexmk -xelatex %f"))
  :config
  ;; add support for coloring code output.  Use minted if pygments is
  ;; installed, otherwise fall back to the listings package, which doesn't
  ;; require anything other than latex.
  (if (executable-find "pygments")
      ;; use minted
      (progn
        (setq org-latex-listings 'minted)
        (add-to-list 'org-latex-packages-alist '("newfloat" "minted"))
        ;; also need to figure out how to add -shell-escape option to `org-latex-pdf-process'
        )
    ;; else use listings
    (progn
      (setq org-latex-listings t)
      (add-to-list 'org-latex-packages-alist '("" "listings"))
      (add-to-list 'org-latex-packages-alist '("" "color"))))

  ;; Add support for writing letters:
  (add-to-list 'org-latex-classes
               '("letter"
                 "\\documentclass[11pt]{letter}
\\signature{J. Alexander Branham}
\\address{}"
                 ("\\section{%s}" . "\\section*{%s}")
                 ("\\subsection{%s}" . "\\subsection*{%s}")
                 ("\\subsubsection{%s}" . "\\subsubsection*{%s}"))))

(progn ; `paragraphs.el'
  ;; I end sentences with a single space.
  (setq sentence-end-double-space nil))

(use-package paren
  :defer 8
  :init
  (setq show-paren-when-point-inside-paren t)
  (setq show-paren-delay 0)
  :config
  (show-paren-mode))

(use-package password-cache
  :defer
  :custom
  (password-cache-expiry 60 "Cache passwords for a minute."))

(use-package pdf-tools
  :disabled
  :straight t
  :if (eq system-type 'gnu/linux)
  :magic ("%PDF" . pdf-view-mode)
  :defer 7
  :custom
  (pdf-view-display-size 'fit-page "Show full pages by default instead of fitting page width.")
  (TeX-view-program-selection '((output-pdf "pdf-tools")) "Use pdf-tools to display pdfs from latex runs.")
  (TeX-view-program-list '(("pdf-tools" ("TeX-pdf-tools-sync-view") nil)))
  :config
  ;; The t says to install the server without asking me --- this may take a
  ;; second
  (pdf-tools-install t))

(use-package prog-mode
  :defer t
  :config
  ;; Prettify-symbols-mode will replace some symbols (like "lambda") with
  ;; their prettier cousins (like λ), but smartly as it's configured by
  ;; major modes themselves.
  (global-prettify-symbols-mode))

(use-package project
  :defer
  :init
  (setq project-vc-merge-submodules nil))

(use-package python
  :defer t
  :bind
  (:map python-mode-map
        ("C-<return>" . my/python-shell-send-region-or-statement-and-step))
  :custom
  ;; Use flake8 for flymake:
  (python-flymake-command '("flake8" "-"))
  (python-indent-guess-indent-offset-verbose nil)
  (python-indent-offset 4)
  (python-eldoc-function-timeout-permanent nil)
  :config
  (defun my/python-shell-send-region-or-statement ()
    "Send the current region to the inferior python process if there is an active one, otherwise the current line."
    (interactive)
    (if (use-region-p)
        (python-shell-send-region (region-beginning) (region-end))
      (my/python-shell-send-statement)))
  (defun my/python-shell-send-statement ()
    "Send the current line to the inferior python process for evaluation."
    (interactive)
    (save-excursion
      (let ((end (python-nav-end-of-statement))
            (beginning (python-nav-beginning-of-statement)))
        (python-shell-send-region beginning end))))
  (defun my/python-shell-send-region-or-statement-and-step ()
    "Call `python-shell-send-region-or-statement' and then `python-nav-forward-statement'."
    (interactive)
    (my/python-shell-send-region-or-statement)
    (python-nav-forward-statement))
  (define-minor-mode my/python-use-ipython-mode
    ;; I don't really get the allure of ipython, but here's something that
    ;; lets me switch back and forth:
    "Make python mode use the ipython interpreter."
    :lighter (" iPy")
    (unless (executable-find "ipython")
      (error "Could not find ipython executable"))
    (if my/python-use-ipython-mode
        ;; activate ipython stuff
        (setq python-shell-buffer-name "Ipython"
              python-shell-interpreter "ipython"
              ;; https://emacs.stackexchange.com/q/24453/115
              ;; https://debbugs.gnu.org/cgi/bugreport.cgi?bug=25306
              python-shell-interpreter-args "--simple-prompt -i")
      ;; else, deactivate everything
      (setq python-shell-buffer-name "Python"
            python-shell-interpreter "python"
            python-shell-interpreter-args "-i"))))

(use-package recentf
  :defer
  :custom
  ;; ivy makes searching through long lists easy, bump these up some from
  ;; the defaults.
  (recentf-max-saved-items 50)
  (recentf-max-menu-items 25)
  :config
  (add-to-list 'recentf-exclude "-autoloads.el"))

(use-package reftex
  :hook
  (LaTeX-mode . turn-on-reftex)
  :custom
  (reftex-cite-format
   '((?\C-m . "\\cite[]{%l}")
     (?t . "\\citet{%l}")
     (?p . "\\citep[]{%l}")
     (?a . "\\autocite{%l}")
     (?A . "\\textcite{%l}")
     (?P . "[@%l]")
     (?T . "@%l [p. ]")
     (?x . "[]{%l}")
     (?X . "{%l}")))
  (reftex-default-bibliography '("~/Sync/bibliography/references.bib")))

(use-package saveplace
  ;; Yes, please save my place when opening/closing files:
  :config
  (save-place-mode))

(use-package server
  :if window-system
  :config
  ;; Start the server if not already running:
  (unless (server-running-p)
    (add-hook 'after-init-hook #'server-start t)))

(use-package sh-script
  :defer t
  :mode
  ("/PKGBUILD$" . sh-mode))

(use-package shell
  :config
  (setenv "PAGER" "cat"))

(use-package simple
  :defer t
  :commands (my/toggle-window-split)
  :hook
  ;; Turn on visual line mode for nice line wrapping
  (after-init . global-visual-line-mode)
  :bind
  ("M-/" . cycle-spacing)
  ("C-z" . undo)
  ;; The -dwim versions of these three commands are new in Emacs 26 and
  ;; better than their non-dwim counterparts, so override those default
  ;; bindings:
  ("M-l" . downcase-dwim)
  ("M-c" . capitalize-dwim)
  ("M-u" . upcase-dwim)
  ;; Super useful for "merging" lines together, overrides the much less
  ;; useful tab-to-tab-stop:
  ("M-i" . delete-indentation)
  ;; `delete-char' doesn't respect e.g. `delete-active-region':
  ([remap delete-char] . delete-forward-char)
  (:prefix-map my/transpose-map
               :prefix "C-t"
               ("f" . my/toggle-window-split)
               ("c" . transpose-chars)
               ("w" . transpose-words)          ;also M-t by default
               ("l" . transpose-lines)
               ("p" . transpose-paragraphs)
               ("s" . transpose-sentences)
               ("x" . transpose-sexps))
  :custom
  (column-number-mode t "Turn on column numbers in mode-line.")
  (delete-active-region 'kill "Single char delete commands kill active regions.")
  (save-interprogram-paste-before-kill t "Save system clipboard before overwriting it.")
  (set-mark-command-repeat-pop t)
  (shell-command-dont-erase-buffer 'end-last-out "Don't erase output in shell buffers since it's so easy to navigate around.")
  (async-shell-command-display-buffer nil "Only show a shell buffer if there's something to show.")
  (kill-ring-max 500)
  (eval-expression-print-length nil)
  (eval-expression-print-level nil)
  (mail-user-agent 'gnus-user-agent)
  (read-mail-command 'gnus)
  :config
  ;; C-o runs `open-line', which I never use and is annoying to hit by accident
  (unbind-key "C-o")
  (defun my/toggle-window-split (&optional arg)
    "Switch between 2 windows split horizontally or vertically.
With ARG, swap them instead."
    (interactive "P")
    (unless (= (count-windows) 2)
      (user-error "Not two windows"))
    ;; Swap two windows
    (if arg
        (let ((this-win-buffer (window-buffer))
              (next-win-buffer (window-buffer (next-window))))
          (set-window-buffer (selected-window) next-win-buffer)
          (set-window-buffer (next-window) this-win-buffer))
      ;; Swap between horizontal and vertical splits
      (let* ((this-win-buffer (window-buffer))
             (next-win-buffer (window-buffer (next-window)))
             (this-win-edges (window-edges (selected-window)))
             (next-win-edges (window-edges (next-window)))
             (this-win-2nd (not (and (<= (car this-win-edges)
                                         (car next-win-edges))
                                     (<= (cadr this-win-edges)
                                         (cadr next-win-edges)))))
             (splitter
              (if (= (car this-win-edges)
                     (car (window-edges (next-window))))
                  'split-window-horizontally
                'split-window-vertically)))
        (delete-other-windows)
        (let ((first-win (selected-window)))
          (funcall splitter)
          (if this-win-2nd (other-window 1))
          (set-window-buffer (selected-window) this-win-buffer)
          (set-window-buffer (next-window) next-win-buffer)
          (select-window first-win)
          (if this-win-2nd (other-window 1))))))
  (defun my/extract-pdf-pages (infile frompg topg)
    "Extracts pages from a pdf file.

Extract pages from INFILE from FROMPG to TOPG using ghostscript.
Output file will be named by appending _pXX-pYY to INFILE."
    (interactive "ffile: \nnfrom: \nnto: ")
    (async-shell-command
     (concat "gs -sDEVICE=pdfwrite -dNOPAUSE -dBATCH -dSAFER"
             " -dFirstPage=" (number-to-string frompg)
             " -dLastPage=" (number-to-string topg)
             " -SOutputFile=" (concat
                               (file-name-sans-extension infile)
                               "_p" (number-to-string frompg)
                               "-p" (number-to-string topg)
                               ".pdf ")
             infile)))
  )                                     ; end use-package simple

(use-package so-long
  :config
  (global-so-long-mode))

(progn ; `subr'
  ;; Emacs has a great system to "narrow" a buffer to just a smaller
  ;; bit. This is useful in a whole bunch of unexpected ways. For example,
  ;; if a function will do something to a whole buffer but you only want to
  ;; apply it to part, you can just narrow to that bit of the buffer. Or
  ;; narrow just to one org subtree when you have a massive org
  ;; document. The narrow commands are a bit confusing by default. This
  ;; cleans them up a bit and makes it more intuitive to use. I got this
  ;; from https://endlessparentheses.com/emacs-narrow-or-widen-dwim.html
  ;; (modified a bit).

  (defun my/narrow-or-widen-dwim (p)
    "Widen if buffer is narrowed, narrow-dwim otherwise.
Dwim means: region, org-src-block, org-subtree, or
defun, whichever applies first.

With prefix P, don't widen, just narrow even if buffer
is already narrowed."
    (interactive "P")
    (cond ((and (buffer-narrowed-p) (not p)) (widen))
          ((region-active-p)
           (narrow-to-region (region-beginning)
                             (region-end)))
          ((derived-mode-p 'org-mode)
           (cond ((ignore-errors (org-narrow-to-block) t))
                 (t (org-narrow-to-subtree))))
          ((derived-mode-p 'latex-mode)
           (LaTeX-narrow-to-environment))
          (t (narrow-to-defun))))

  ;; This line actually replaces Emacs' entire narrowing
  ;; keymap, that's how much I like this command. Only
  ;; copy it if that's what you want.
  (bind-key* "C-x n" #'my/narrow-or-widen-dwim))

(use-package swiper
  :defer t
  :bind
  ("M-s o" . swiper-isearch))

(use-package text-mode
  :hook
  (text-mode . my/dubcaps-mode)
  :commands (my/dubcaps-mode)
  :init
  (defun my/dcaps-to-scaps ()
    "Convert word in DOuble CApitals to Single Capitals."
    (interactive)
    (and (= ?w (char-syntax (char-before)))
         (save-excursion
           (and (if (called-interactively-p 'any)
                    (skip-syntax-backward "w")
                  (= -3 (skip-syntax-backward "w")))
                (let (case-fold-search)
                  (looking-at "\\b[[:upper:]]\\{2\\}[[:lower:]]"))
                (capitalize-word 1)))))
  (define-minor-mode my/dubcaps-mode
    "Toggle `my/dubcaps-mode'.

Converts words in DOuble CApitals to Single Capitals as you
type."
    :init-value nil
    :lighter (" DC")
    (if my/dubcaps-mode
        (add-hook 'post-self-insert-hook #'my/dcaps-to-scaps nil 'local)
      (remove-hook 'post-self-insert-hook #'my/dcaps-to-scaps 'local))))

(use-package tooltip
  :defer t
  :config
  (setq tooltip-resize-echo-area t)
  ;; If the mouse goes over a divider between windows, Emacs helpfully
  ;; tells you what pressing the mouse buttons will do.  This is a little
  ;; annoying, though, so let's disable it:
  (tooltip-mode -1))

(use-package transient
  :defer t
  :custom
  (transient-read-with-initial-input nil))

(use-package tramp
  ;; TRAMP allows me to visit remote files in my local Emacs instance.  It's
  ;; pretty sweet.
  :defer t
  :custom
  (tramp-histfile-override nil "Don't leave histfiles everywhere.")
  (tramp-default-method "ssh" "Use ssh by default.")
  :config
  (add-to-list 'tramp-remote-path "/home/alex/.nix-profile/bin"))

(use-package unfill
  :straight t
  ;; fill-paragraph is nice, but emacs weirdly lacks a convenient way to
  ;; unfill paragraphs once they're filled.  This package adds that
  ;; functionality.
  :bind
  ([remap fill-paragraph] . unfill-toggle))

(use-package vc-git
  :defer t
  :bind
  ("C-x M-g l" . my/vc-history-line)
  :hook
  (vc-git-log-view-mode . bug-reference-mode)
  :config
  (defun my/vc-history-line ()
    "Show history of current line."
    (interactive)
    (vc-region-history (line-beginning-position) (line-end-position))))

(use-package vc-hooks
  :defer t
  :custom
  (vc-follow-symlinks t "Don't ask to follow symlinks.")
  (vc-make-backup-files t "Always make backup files.  Of everything.  Always."))

(progn ; `window.el'
  ;; start maximized
  (add-to-list 'default-frame-alist '(fullscreen . maximized))
  ;; `display-buffer-alist' determines how and where buffers are displayed:
  (bind-key "C-c w" #'window-toggle-side-windows)
  (defun my/display-buffer-fullframe (buffer alist)
    "Display BUFFER using ALIST."
    ;; from `magit--display-buffer-fullframe':
    (let ((window (or (display-buffer-reuse-window buffer alist)
                      (display-buffer-same-window buffer alist)
                      (display-buffer-pop-up-window buffer alist)
                      (display-buffer-use-some-window buffer alist))))
      (when window
        (delete-other-windows window)
        window)))
  (unless (fboundp 'display-buffer-in-direction)
    (defalias 'display-buffer-in-direction #'display-buffer-in-side-window))
  (defun my/display-window-at-right (buffer alist)
    "Display BUFFER using ALIST."
    (require 'subr-x)
    (if-let ((window (and (bound-and-true-p ess-local-process-name)
                          (get-buffer-window (ess-get-process-buffer)))))
        (display-buffer-in-direction buffer `(nil (direction . below)
                                                  (window . ,window)
                                                  (side . right)
                                                  (slot . 1)
                                                  (window-height . 50)))
      (display-buffer-in-direction buffer `(nil (direction . rightmost)
                                                (side . right)
                                                (slot . 1)))))
  (setq display-buffer-alist
        ;; Help and stuff at the right
        `((,(rx string-start (or "*Apropos"
                                 "*Backtrace"
                                 "*Compile-Log*"
                                 "*Ledger Report"
                                 "*Man"
                                 "*Process List*"
                                 "*Python"
                                 "*R:"
                                 "*Warnings*"
                                 "*WoMan"
                                 "*compilation"
                                 "*eglot help"
                                 (and (0+ anything) ".pdf")
                                 (and (1+ not-newline) " output*"))) ; AUCTeX
           (display-buffer-reuse-window display-buffer-reuse-mode-window display-buffer-in-direction)
           (direction . rightmost)
           (side . right)
           (window-width . 80)
           (window-height . 0.45))
          ;; Side window on bottom:
          (,(rx string-start (or "*Calendar"
                                 "*ert"
                                 "*Reconcile"))
           (display-buffer-reuse-window display-buffer-in-direction)
           (side . bottom)
           (direction . bottom))
          ;; Right side, below the inferior buffer
          (,(rx string-start (or "*help"
                                 "R_x11"))
           (display-buffer-reuse-window display-buffer-reuse-mode-window my/display-window-at-right)
           (window-width . 80))
          ;; Full frame
          (,(rx string-start "magit: ")
           (display-buffer-reuse-window my/display-buffer-fullframe))
          ;; Right side, above the inferior buffer
          (,(rx string-start "*R dired")
           (display-buffer-reuse-window display-buffer-in-direction)
           (direction . rightmost)
           (side . right)
           (slot . -1)
           (window-height . 10))
          ;; Use same window
          (,(rx string-start (or "*Annotate "
                                 "*edit-indirect"
                                 "magit-log: "
                                 "magit-refs: "
                                 "*shell"))
           (display-buffer-reuse-window display-buffer-same-window))))
  ;; Don't split windows vertically
  (setq split-height-threshold nil)
  ;; resize windows:
  (bind-keys ("S-C-<left>" . shrink-window-horizontally)
             ("S-C-<right>" . enlarge-window-horizontally)
             ("S-C-<down>" . shrink-window)
             ("S-C-<up>" . enlarge-window))
  ;; These functions make splitting windows behave more like I want it to.
  ;; This way, calling C-x 2 or C-x 3 both splits the window *and* shows
  ;; the last buffer.
  (defun my/vsplit-last-buffer (prefix)
    "Split the window vertically and display the previous buffer."
    (interactive "p")
    (split-window-vertically)
    (other-window 1 nil)
    (if (= prefix 1)
        (switch-to-next-buffer)))
  (defun my/hsplit-last-buffer (prefix)
    "Split the window horizontally and display the previous buffer."
    (interactive "p")
    (split-window-horizontally)
    (other-window 1 nil)
    (if (= prefix 1) (switch-to-next-buffer)))
  (bind-keys ("C-x 2" . my/vsplit-last-buffer)
             ("C-x 3" . my/hsplit-last-buffer))
  ;; the defaults C-v and M-v scroll a full page, which is too much.
  ;; rebind to a half page:
  (defun my/scroll (arg)
    "Scroll a half page down.
With ARG, scroll up instead."
    (interactive "P")
    (let ((count (/ (1- (window-height)) 2)))
      (if arg
          (scroll-up count)
        (scroll-down count)))
    (set-transient-map
     (let ((map (make-sparse-keymap)))
       (define-key map (kbd "v") #'my/scroll-up)
       (define-key map (kbd "M-v") #'my/scroll)
       map)))
  (defun my/scroll-up ()
    "See `my/scroll'."
    (interactive)
    (my/scroll 'up))
  (bind-keys ("C-v" . my/scroll-up)
             ("M-v" . my/scroll))
  ;; To quickly access scratch press my/map f1:
  (defun my/get-scratch ()
    "Switch to scratch buffer."
    (interactive)
    (switch-to-buffer "*scratch*"))
  (bind-key "<f1>" #'my/get-scratch my/map))

(use-package winum
  :straight t
  :defer 1
  ;; I can use winum to quickly jump from window to window.
  :bind*
  ("M-0" . winum-select-window-0-or-10)
  ("M-1" . winum-select-window-1)
  ("M-2" . winum-select-window-2)
  ("M-3" . winum-select-window-3)
  ("M-4" . winum-select-window-4)
  ("M-5" . winum-select-window-5)
  ("M-6" . winum-select-window-6)
  ("M-7" . winum-select-window-7)
  ("M-8" . winum-select-window-8)
  ("M-9" . winum-select-window-9)
  :config
  (setq winum-scope 'frame-local)
  (setq winum-auto-setup-mode-line nil)
  (winum-mode))

(use-package with-editor
  :straight t
  ;; Use Emacs as the $EDITOR environmental variable:
  :hook
  ((shell-mode eshell-mode) . with-editor-export-editor)
  :config
  (shell-command-with-editor-mode))

(use-package ws-butler
  :straight t
  ;; Whitespace is evil.  Let's get rid of as much as possible.  But we
  ;; don't want to do this with files that already had whitespace (from
  ;; someone else's project, for example).  This mode will call
  ;; `whitespace-cleanup' before buffers are saved (but smartly)!
  :hook
  ((prog-mode ledger-mode gitconfig-mode) . ws-butler-mode)
  :custom
  (ws-butler-keep-whitespace-before-point nil))

(use-package winner
  ;; `winner-mode' lets me undo/redo changes to the window configuration,
  ;; bound to C-c left and C-c right by default.
  :defer 1
  :config
  (winner-mode))

(use-package woman
  :defer t
  :bind
  ("C-h a" . woman)
  :custom-face
  (woman-bold ((t (:foreground "MediumPurple" :inherit bold)))))

;; Restore gc threshold
(run-with-idle-timer 5 nil
                     (lambda () (setq gc-cons-threshold 800000)))

;; Local Variables:
;; indent-tabs-mode: nil
;; fill-column: 75
;; sentence-end-double-space: t
;; no-byte-compile: t
;; coding: utf-8
;; End:

;;; init.el ends here
